import React, { useState, useEffect } from "react";
import { View, Swiper, SwiperItem } from "@tarojs/components";

const BannerAD = (props) => {
  useEffect(() => {
    
  }, [])

  return (
    <View className="adBannerContainer">
      <Swiper
        current={0}
        className="bannerSwiper"
        indicatorColor="#C2C2C2"
        indicatorActiveColor="#EBEBEB"
        circular
        indicatorDots
        autoplay
      >
        <SwiperItem>
          12312312312
        </SwiperItem>
      </Swiper>
    </View>
  );
};

export default React.memo(BannerAD);
